new Vue({
    el: '#desafio',
    data: {
        valor: ''
    },
    methods: {
      exibirAlerta() {
        alert('Alerta exibido!');
      },
      mostrarValor(e) {
        this.valor = e.target.value;
      },
    },
})

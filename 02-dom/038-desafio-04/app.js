new Vue({
	el: '#desafio',
	data: {
    ativar: false,
    alterna: false,
    mudarEfeito: 'destaque',
    intervalo: 0,
    c1: 'classe01',
    c2: 'classe02',
    classeDigitada: 'destaque',
    ativarClasse: false,
    classeUsuario1: 'classeUsuario1',
    mudarEstilo: 'width:100px;height:100px;border:1px solid black;',
    valorBarra: 0,
    completo: '',
	},
	methods: {
		iniciarEfeito() {
      this.ativar = !this.ativar;
      if(this.ativar) {
        this.intervalo = setInterval(() => {
          if (this.alterna) {
            this.mudarEfeito = "destaque";
          } else {
            this.mudarEfeito = "encolher";
          }
          this.alterna = !this.alterna;
        }, 1000);
      } else {
        clearInterval(this.intervalo);
      }
		},
		iniciarProgresso() {
      let barra = true;
      this.valorBarra = 0;
      this.completo = 'baixando...';
      const intervalo2 = setInterval(() => {
        if(barra) {
          if (this.valorBarra < 100) {
            this.valorBarra++;
          } else {
            barra = false;
            this.completo = 'Finalizado!';
          }
        } else {
          clearInterval(intervalo2);
        }
      }, 50);
		}
	},
});

new Vue({
  el: "#desafio",
  data: {
    nome: 'Temistocles',
    idade: 38,
    imagemSpace: 'https://lh3.googleusercontent.com/Ts_JDDrH_7Iat5_SDixVs02JU4qT-HaQhTSmg0bG6BUcNrdYCpFkW4bTj8y5_csC',
  },
  methods: {
    idadeVezes3() {
      return this.idade * 3;
    },
    random() {
      return Math.random();
    }
  }
});
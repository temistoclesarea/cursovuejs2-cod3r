new Vue({
    el: '#desafio',
    data: {
        valor: 0,
    },
    computed: {
      resultado() {
        /* if (this.valor === 37) {
          return 'Valor Igual';
        } else {
          return 'Valor Diferente';
        } */
        return this.valor == 37 ? 'Valor Igual':'Valor Diferente';
      },
    },
    watch: {
      resultado() { // obs. a mudança só acontece quando ocorre uma alteração no valor de resultado
        setTimeout(() => {
          this.valor = 0;
        }, 5000);
      }
    },
});
